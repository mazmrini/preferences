alias python='/usr/bin/python3'
alias pip='/usr/bin/pip3'

java8() {
  export JAVA_HOME="`/usr/libexec/java_home -v 1.8`"
}
java11() {
  export JAVA_HOME="`/usr/libexec/java_home -v 11.0`"
}
java11

alias ll='ls -la'
alias ..='cd ..'

alias cdperso='cd ~/files/perso'
alias cdkasbah='cd ~/files/perso/garderie'
alias cdpref='cd ~/files/perso/preferences'

alias cdwork='cd ~/files/work'
alias todo='emacs ~/files/work/current_work.org'

alias gitonlymain='git checkout main && git branch | grep -v "main" | xargs git branch -D && git fetch --all -p && gitrebasemain'
alias gitonlymaster='git checkout master && git branch | grep -v "master" | xargs git branch -D && git fetch --all -p && gitrebasemaster'
alias gitrebasemaster='git fetch origin && git rebase origin/master'
alias gitrebasemain='git fetch origin && git rebase origin/main'
alias gitloggraph='git log --graph --decorate --oneline'

# takes 1 arg as input
shrimpify() {
  python /Users/mazine/files/perso/jupyter/scripts/shrimpify.py $1
}