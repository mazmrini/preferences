if [ -f ~/.zsh.d/.bash_env ]; then
    . ~/.zsh.d/.bash_env
fi

if [ -f ~/.zsh.d/.bash_aliases ]; then
    . ~/.zsh.d/.bash_aliases
fi

if [ -f ~/.zsh.d/.bash_completion ]; then
    . ~/.zsh.d/.bash_completion
fi

parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/';
}

setopt PROMPT_SUBST

export PS1='%F{191}mazine@:%F{111}%~%F{203}$(parse_git_branch)%F{default}$ '

[ -f /opt/dev/dev.sh ] && source /opt/dev/dev.sh
if [ -e /Users/mazine/.nix-profile/etc/profile.d/nix.sh ]; then . /Users/mazine/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

[[ -f /opt/dev/sh/chruby/chruby.sh ]] && type chruby >/dev/null 2>&1 || chruby () { source /opt/dev/sh/chruby/chruby.sh; chruby "$@"; }

[[ -x /usr/local/bin/brew ]] && eval $(/usr/local/bin/brew shellenv)

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/mazine/Downloads/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/mazine/Downloads/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/mazine/Downloads/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/mazine/Downloads/google-cloud-sdk/completion.zsh.inc'; fi

autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /opt/terraform/bin/terraform terraform
