if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

export PS1="\[\033[33m\]mazine@:\[\033[34m\]\w\[\033[31m\]\$(parse_git_branch)\[\033[00m\]$ "
