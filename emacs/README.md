# Emacs manual setup

### Neotree

Download it using:
```
M-x package-list-packages (use ESC for M on mac)
C-s neotree
i (select it)
x (confirm install)
```

Activate it through `C-x RETURN`