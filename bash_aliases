alias ll='ls -la'

alias cdperso='cd ~/files/perso'
alias cdpref='cd ~/files/perso/preferences'

alias cdwork='cd ~/files/work'
alias cdacs='cd ~/files/work/curbside-acs'
alias cddevops='cd ~/files/work/curbside-devops'
alias cdgw='cd ~/files/work/curbside-gw'

alias gitonlymaster='git branch | grep -v "master" | xargs git branch -D'
